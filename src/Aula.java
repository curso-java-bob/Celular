import objetos.Aluno;
import objetos.Disciplina;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Aula {
    public static void main(String[] args) {

        List<Aluno> alunos = new ArrayList<Aluno>();

        for (int qnt = 1; qnt <= 2; qnt++) {
            String nome = JOptionPane.showInputDialog(" Nome do aluno " + qnt + " ? ");
            String idade = JOptionPane.showInputDialog(" Idade do aluno " + qnt + " ? ");
            String nascimento = JOptionPane.showInputDialog(" Nascimento do aluno " + qnt + " ? ");
            String escola = JOptionPane.showInputDialog(" Escola do aluno " + qnt + " ? ");

            Aluno aluno1 = new Aluno();
            aluno1.setNome(nome);
            aluno1.setIdade(Integer.valueOf(idade));
            aluno1.setNascimento(nascimento);
            aluno1.setEscola(escola);


            for (int pos = 1; pos <= 4; pos++) {

                String nomeDisciplina = JOptionPane.showInputDialog("Nome da disciplina" + pos + " ? ");
                String notaDisciplina = JOptionPane.showInputDialog("Nota da disciplina" + pos + " ? ");

                Disciplina discipliana = new Disciplina();
                discipliana.setDisciplina(nomeDisciplina);
                discipliana.setNota(Double.parseDouble(notaDisciplina));

                aluno1.getDisciplinas().add(discipliana);

            }
            int escolha = JOptionPane.showConfirmDialog(null, "Deseja remover alguma disciplina?");

            if (escolha == 0) {

                int continuarRemover = 0;


                while (continuarRemover == 0) {

                    String disciplinaRetirar = JOptionPane.showInputDialog("Qual disciplina deseja remover ? ");

                   /* for (int i = 0; i <= aluno1.getDisciplinas().size(); i++) {



                        if (disciplinaRetirar.equals(aluno1.getDisciplinas().get(i).getDisciplina())) {

                            aluno1.getDisciplinas().remove(aluno1.getDisciplinas().get(i));

                        }
                    }*/

                    for (Disciplina disciplina : aluno1.getDisciplinas()) {


                        if (disciplinaRetirar.equals(disciplina.getDisciplina())) {
                            aluno1.getDisciplinas().remove(disciplina);

                            break;
                        }
                    }

                    continuarRemover = JOptionPane.showConfirmDialog(null, "Continuar a remover?");


                }


            }


            alunos.add(aluno1);


        }
        for (Aluno aluno : alunos) {

            System.out.println("Média do aluno = " + aluno.getMediaNota());
            System.out.println("Resultado = " + aluno.getAlunoResultado());
            System.out.println("disciplinas" + aluno.getDisciplinas());


        }


    }
}
