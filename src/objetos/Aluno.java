package objetos;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Aluno {

    private String nome;
    private int idade;
    private String nascimento;
    private String escola;

    private List<Disciplina> disciplinas = new ArrayList<Disciplina>();


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public String getEscola() {
        return escola;
    }

    public void setEscola(String escola) {
        this.escola = escola;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    /* METODO QUE RETORNA A MEDIA DO ALUNO */

    public double getMediaNota() {

        double somaNotas = 0.0;

        for (Disciplina disciplina : disciplinas) {

            somaNotas += disciplina.getNota();


        }

        return somaNotas / disciplinas.size();


    }

    /* METODO PARA RETORNAR TRUE APROVADO E FALSE REPROVADO */


    public String getAlunoResultado() {
        double media = this.getMediaNota();
        if (media >= 50) {
            if (media >= 70) {
                return "Aluno aprovado";
            } else {
                return "Aluno em recuperação";
            }

        } else {
            return "Aluno reprovado";
        }

    }


    @Override
    public String toString() {
        return "Aluno{" +
                "nome='" + nome + '\'' +
                ", idade=" + idade +
                ", nascimento='" + nascimento + '\'' +
                ", escola='" + escola + '\'' +
                ", disciplinas=" + disciplinas +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Aluno)) return false;
        Aluno aluno = (Aluno) o;
        return idade == aluno.idade && Objects.equals(nome, aluno.nome) && Objects.equals(nascimento, aluno.nascimento) && Objects.equals(escola, aluno.escola) && Objects.equals(disciplinas, aluno.disciplinas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, idade, nascimento, escola, disciplinas);
    }
}






